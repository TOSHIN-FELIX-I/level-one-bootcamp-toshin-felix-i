//WAP to find the volume of a tromboloid using 4 functions.
/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>

float computeVT(int x, int y, int z);
int inputVal();
void outputVal(float vt);

int main()
{
    int  h, b, d;
    float V;
    //Read a  value for Height+
    h=inputVal();
    
    //Read a value for Depth:
    d=inputVal();
    
    //Read a value for Breadth:
    b=inputVal();
    
    V=computeVT(h,d,b);
    
    outputVal(V);
}
int inputVal()
{
    int x;
    printf("Enter a value:");
    scanf("%d", &x);
    return x;
}

float computeVT(int x, int y, int z)
{
    float computevt;
    computevt=((x*y)+y)/(3*z);
    return(computevt);
}

void outputVal(float x)
{
     printf("Volume of a tromboid= %f\n", x);
}