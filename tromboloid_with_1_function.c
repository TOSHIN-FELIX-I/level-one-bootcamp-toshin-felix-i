//Write a program to find the volume of a tromboloid using one function

#include <stdio.h>

float VT(int x, int y, int z);

int main()
{
    int  h, b, d;
    float V;
    
    printf("Enter the value for Height: \n");
    scanf("%d", &h);
    
    printf("Enter the value for Depth: \n");
    scanf("%d", &d);
    
    printf("Enter the value for Breadth: \n");
    scanf("%d", &b);
    V = VT(h,d,b);
    
    printf("Volume of a tromboid= %f\n", V);
}

float VT(int x, int y, int z)
{
    float vt;
    vt=((x*y)+y)/(3*z);
    return(vt);
}
