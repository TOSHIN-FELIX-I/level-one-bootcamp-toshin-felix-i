//Write a program to find the sum of n different numbers using 4 functions

#include <stdio.h>

int main()
{
    int n, totalsum = 0, i, x;
    
    printf("Enter the Number of no.s You want to to add: \n");
    scanf("%d", &n);
    
    printf("Enter %d integers\n", n);
    
     for (i = 1; i <=n; i++)
     {
         scanf("%d", &x);
         totalsum = totalsum + x;
     }
    
     printf("Sum of the integers= %d\n", totalsum);
    
     return 0;
}
    

 

